package Conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoDb {
	private static final String URL_MYSQL = "jdbc:mysql://localhost/Biblioteca?autoReconnect=true&useSSL=false";
	
	private static final String USER = "root";
	
	private static final String PASS = "mysqlroot";

	public static Connection getConnection() {
		System.out.println("Conectando ao Banco de Dados");
		try {
			return DriverManager.getConnection(URL_MYSQL, USER, PASS);
			} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Nao conectou");			
		}
		return null;
	}
}