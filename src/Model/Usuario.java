package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class Usuario {
	
	private int id;
	private String nome;
	private String login;
	private String senha;
	
	public Usuario() {
		
	}
	public Usuario(int id, String nome, String login, String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.login = login;
		this.senha = senha;
	}
	public int getid() {
		return id;
	}
	public void setid() {
		 this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public boolean logar(String user, String senha, Connection conn) {
		
		String sqlSelect = "SELECT * FROM Usuario WHERE loginUsuario = ?;";
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		try {
			
			stm = conn.prepareStatement(sqlSelect);
			stm.setString(1, user);
			rs = stm.executeQuery();
			
			if(rs.next()) {
				 if(rs.getString(3).equalsIgnoreCase(user)) {
					 if(rs.getString(4).equalsIgnoreCase(senha)) {
						 JOptionPane.showMessageDialog(null,"Login concluido. Bem vindo " + user + ".");
						 return true;
					 }
					 else {
						 JOptionPane.showMessageDialog(null,"Senha incorreta.");
						 return false;
					 }
				 }
				 else {
					 JOptionPane.showMessageDialog(null,"Usuario incorreto.");
					 return false;
				 }
			}
			stm.close();
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public void cadastrar(Connection conn) {
		String sqlInsert = "INSERT INTO Usuario VALUES (?,?,?,?)";
		PreparedStatement stm = null;
		
		try {
			stm = conn.prepareStatement(sqlInsert);
			
			stm.setString(1, Integer.toString(getid()));
			stm.setString(2, getNome());
			stm.setString(3, getLogin());
			stm.setString(4, getSenha());
			
			stm.execute();
			stm.close();
			JOptionPane.showMessageDialog(null,"Usuario cadastrado.");
		
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deletar(Connection conn, String login) {
		
		String sqlDelete = "DELETE FROM Usuario WHERE Usuario.loginUsuario = ?	";
		PreparedStatement stm = null;
		
		try {
			stm = conn.prepareStatement(sqlDelete);

			stm.setString(1, getLogin());
			stm.execute();
			stm.close();
			
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null,"Não foi possivel excluir o usuario.");
			
			try {
				conn.rollback();
			} catch (SQLException f) {
				System.out.println(f.getStackTrace());
			}
		}finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException g) {
					System.out.print(g.getStackTrace());
				}
			}
		}
	}
	
	public void alterar(Connection conn) {
		String sqlUpdate = "UPDATE Usuario SET nomeUsuario = ?, loginUsuario = ?, senhaUsuario = ? WHERE idUsuarios = ?";

		PreparedStatement stm = null;
		
		try {
			stm = conn.prepareStatement(sqlUpdate);

			stm.setString(1, getNome());
			stm.setString(2, getLogin());
			stm.setString(3, getSenha());
			stm.setString(4, Integer.toString(getid()));
			
			stm.execute();
			
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null,"Não foi possivel alterar o usuario.");
			
			try {
				conn.rollback();
			} catch (SQLException e9) {
				System.out.print(e9.getStackTrace());
			}
		}finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException e9) {
					System.out.print(e9.getStackTrace());
				}
			}
		}
	}

}
