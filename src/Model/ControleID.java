package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ControleID {
	
	public ControleID() {
		
	}
	
	public void salvaId(int ido, Connection conn, String table) {
		
		String sqlUpdate = "";
		if(table == "ID") {
			sqlUpdate = "UPDATE ID SET id=?;";
		}else if(table == "idLivros"){
			sqlUpdate = "UPDATE idLivros SET id=?;";
		}
		PreparedStatement stm = null;
		
		try {
			stm = conn.prepareStatement(sqlUpdate);
			stm.setString(1, Integer.toString(ido));
			stm.execute();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public int verificaId(Connection conn, String table) {
		String sqlSelect = ""; 
		
		if(table == "ID") {
			sqlSelect = "SELECT * FROM ID;";
		}else if(table == "idLivros") {
			sqlSelect = "SELECT * FROM idLivros;";
		}
		
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		try {
			stm = conn.prepareStatement(sqlSelect);
			rs = stm.executeQuery();
			System.out.println("Entrou no try");
			
			if(rs.next()) {
				String temp = rs.getString(1);
				System.out.println("Valor:" + temp);
				return Integer.parseInt(temp);
			}
			stm.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
