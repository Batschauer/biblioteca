package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JOptionPane;

public class Livro {
	
	private int idLivro;
	private String titulo;
	private String autor;
	private String genero;
	private String editora;
	
	public Livro() {
		
	}
	
	public Livro(int idLivro, String titulo, String autor, String genero, String editora) {
		super();
		this.idLivro = idLivro;
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
		this.editora = editora;
	}

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public int getIdLivro() {
		return idLivro;
	}
	public void setIdLivro(int idLivro) {
		this.idLivro = idLivro;
	}
	//inserir livro
		public void inserirLivro ( Connection conn){
			String sqlIncluir = "INSERT INTO Livros VALUES (?,?,?,?,?);";
			PreparedStatement stm = null;
			
			try {
				stm = conn.prepareStatement(sqlIncluir);
				
				stm.setString(1, Integer.toString(getIdLivro()));
				stm.setString(2, getTitulo());
				stm.setString(3, getAutor());
				stm.setString(4, getGenero());
				stm.setString(5, getEditora());
				
				stm.execute();
				stm.close();
				
				JOptionPane.showMessageDialog(null, "Livro inserido com sucesso!");
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Falha!");
				e.printStackTrace();
			}
		}
		
		
	//Buscar livro
	public void buscarLivro (Connection conn) {
		String sqlSelect = "SELECT  `idLivro`,`titulo`, `autor`, `editora`, `genero`"
				+ " FROM `Biblioteca` WHERE idLivro = ?";
		
		PreparedStatement stm = null;
		
		ResultSet rs = null;
		
		try {
			stm = conn.prepareStatement(sqlSelect);
			
			rs = stm.executeQuery();
			
				stm.setInt(1, getIdLivro());
				
			if (rs.next()) {
				this.setIdLivro(rs.getInt(1));
				this.setTitulo(rs.getString(2));
				this.setAutor(rs.getString(3));
				this.setEditora(rs.getString(4));
				this.setGenero(rs.getString(5));
			}
			JOptionPane.showMessageDialog(null, "Buscado com sucesso!");
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Falha!");
		}
		finally {
			if (stm != null) {
				if (rs != null)
				try {
					stm.close();
					rs.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}
	}
	
	
	//Alterar Livro
	public void alterarLivro ( Connection conn){
		String sqlAlterar = "UPDATE PET SET titulo = ?, autor = ?, genero = ?, editora = ? WHERE idLivro = ?";
		
		PreparedStatement stm = null;
		
		try {
			stm = conn.prepareStatement(sqlAlterar);
			
			stm.setString(1, getTitulo());
			stm.setString(2, getAutor());
			stm.setString(3, getGenero());
			stm.setString(4, getEditora());
			
			stm.execute();
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Falha!");
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		} finally {
			if (stm != null) {
				try {
					stm.close();
					System.out.println("alterado");
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}		
	}
	
	
	// Excluir Livro
		public void excluirLivro (Connection conn){
			String sqlExcluir = "DELETE FROM Biblioteca WHERE livro.idLivro = ?";
			PreparedStatement stm = null;
			
			try {
				stm = conn.prepareStatement(sqlExcluir);
				stm.setInt(1, getIdLivro());
				stm.execute();
				stm.close();
				
				JOptionPane.showMessageDialog(null, "Excluido com sucesso!");
			} catch (Exception e) {
				e.printStackTrace();
				try {
					conn.rollback();
				} catch (Exception e2) {
					e2.printStackTrace();			}
			}finally {
				if (stm != null) {
					try {
						stm.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
		}
}
