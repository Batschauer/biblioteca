package View;

import Conn.ConexaoDb;
import Model.ControleID;
import Model.Usuario;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.awt.event.ActionEvent;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField tfLogin;
	private JTextField tfSenha;
	private JTextField tfNomeAdd;
	private JTextField tfLoginAdd;
	private JTextField tfSenhaAdd;
	private int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() throws IOException {
		
		//armazenar o id
		FileWriter arquivoW = new FileWriter("C:\\Users\\Natan\\Documents\\Biblioteca\\biblioteca\\id.txt");
	    PrintWriter gravarArquivo = new PrintWriter(arquivoW);
	    
	    //ler o id
	    FileReader arquivoR = new FileReader("C:\\Users\\Natan\\Documents\\Biblioteca\\biblioteca\\id.txt");
	    BufferedReader lerArquivo = new BufferedReader(arquivoR);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 335);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 24, 424, 262);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Entrar", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(10, 11, 404, 114);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblUsurio = new JLabel("Usu\u00E1rio");
		lblUsurio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsurio.setBounds(20, 25, 55, 20);
		panel_1.add(lblUsurio);
		
		tfLogin = new JTextField();
		tfLogin.setBounds(105, 27, 144, 20);
		panel_1.add(tfLogin);
		tfLogin.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSenha.setBounds(20, 61, 55, 20);
		panel_1.add(lblSenha);
		
		tfSenha = new JTextField();
		tfSenha.setBounds(105, 63, 144, 20);
		panel_1.add(tfSenha);
		tfSenha.setColumns(10);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Botão para logar
				
				String user, senha;
				Connection conn = null;
				
				try {
					ConexaoDb minhaConecao = new ConexaoDb();
					conn = minhaConecao.getConnection();
					conn.setAutoCommit(false);
					
					user = tfLogin.getText();
					senha = tfSenha.getText();
					
					Usuario us = new Usuario();
					
					if(us.logar(user, senha, conn) == true) {
						new Escolha().setVisible(true);
						Login.this.dispose();
					}
					
				}catch(Exception e) {
					JOptionPane.showMessageDialog(null,"Tente mais uma vez.");
				}
			}
			
		});
		btnEntrar.setBounds(287, 41, 89, 23);
		panel_1.add(btnEntrar);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Inscreva-se", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(10, 124, 404, 148);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblNomeCompleto = new JLabel("Nome");
		lblNomeCompleto.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNomeCompleto.setBounds(21, 28, 45, 22);
		panel_2.add(lblNomeCompleto);
		
		tfNomeAdd = new JTextField();
		tfNomeAdd.setBounds(102, 30, 140, 20);
		panel_2.add(tfNomeAdd);
		tfNomeAdd.setColumns(10);
		
		JLabel lblUsurio_1 = new JLabel("Usu\u00E1rio");
		lblUsurio_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUsurio_1.setBounds(20, 61, 46, 14);
		panel_2.add(lblUsurio_1);
		
		tfLoginAdd = new JTextField();
		tfLoginAdd.setBounds(102, 59, 140, 20);
		panel_2.add(tfLoginAdd);
		tfLoginAdd.setColumns(10);
		
		JLabel lblSenha_1 = new JLabel("Senha");
		lblSenha_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSenha_1.setBounds(21, 90, 46, 14);
		panel_2.add(lblSenha_1);
		
		tfSenhaAdd = new JTextField();
		tfSenhaAdd.setBounds(102, 88, 140, 20);
		panel_2.add(tfSenhaAdd);
		tfSenhaAdd.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Botão para cadastrar usuario
				
				String nome, login, senha;
				Connection conn = null;
				ControleID temp1 = new ControleID();
				String table = "ID";
				
				try {
					
					ConexaoDb minhaConecao = new ConexaoDb();
					conn = minhaConecao.getConnection();
					conn.setAutoCommit(false);
					
					int temp2 = temp1.verificaId(conn, table);
					id = temp2 + 1;
					temp1.salvaId(id, conn, table);
					
					System.out.println(id);
					
					nome = tfNomeAdd.getText();
					login = tfLoginAdd.getText();
					senha = tfSenhaAdd.getText();
					
					Usuario us = new Usuario(id,nome, login, senha);
					us.cadastrar(conn);
					conn.commit();
					
					tfNomeAdd.setText("");
					tfLoginAdd.setText("");
					tfSenhaAdd.setText("");
					
					
					
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		});
		btnCadastrar.setBounds(291, 58, 89, 23);
		panel_2.add(btnCadastrar);
		
		JLabel label = new JLabel("Sistema Bibliotec\u00E1rio");
		label.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		label.setBounds(117, 0, 209, 24);
		contentPane.add(label);
		
	}
}
