package View;

import Model.ControleID;
import Model.Livro;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Conn.ConexaoDb;


public class CrudLivros extends JFrame {

	private JPanel contentPane;
	private JTextField tfTitiloIncluir;
	private JTextField tfAutorIncluir;
	private JTextField tfGeneroIncluir;
	private JTextField tfEditoraIncluir;
	private JTextField tfTituloBuscar;
	private JTextField tfAutorBuscar;
	private JTextField tfGeneroBuscar;
	private JTextField tfEditoraBuscar;
	private JTable table;
	private int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrudLivros frame = new CrudLivros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrudLivros() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(Color.CYAN);
		tabbedPane.setBounds(10, 21, 514, 229);
		contentPane.add(tabbedPane);
		
		JPanel Incluir = new JPanel();
		tabbedPane.addTab("Incluir", null, Incluir, null);
		Incluir.setLayout(null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBorder(new TitledBorder(null, "Adicionar Livro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(0, 0, 409, 211);
		Incluir.add(panel_4);
		
		JLabel label = new JLabel("T\u00EDtulo");
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label.setBounds(20, 25, 46, 14);
		panel_4.add(label);
		
		tfTitiloIncluir = new JTextField();
		tfTitiloIncluir.setColumns(10);
		tfTitiloIncluir.setBounds(112, 23, 86, 20);
		panel_4.add(tfTitiloIncluir);
		
		JLabel label_1 = new JLabel("Autor");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_1.setBounds(20, 74, 46, 14);
		panel_4.add(label_1);
		
		tfAutorIncluir = new JTextField();
		tfAutorIncluir.setColumns(10);
		tfAutorIncluir.setBounds(112, 72, 86, 20);
		panel_4.add(tfAutorIncluir);
		
		JLabel label_3 = new JLabel("G\u00EAnero");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_3.setBounds(20, 123, 46, 14);
		panel_4.add(label_3);
		
		tfGeneroIncluir = new JTextField();
		tfGeneroIncluir.setColumns(10);
		tfGeneroIncluir.setBounds(112, 121, 86, 20);
		panel_4.add(tfGeneroIncluir);
		
		JLabel label_4 = new JLabel("Editora");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_4.setBounds(20, 169, 46, 14);
		panel_4.add(label_4);
		
		tfEditoraIncluir = new JTextField();
		tfEditoraIncluir.setColumns(10);
		tfEditoraIncluir.setBounds(112, 167, 86, 20);
		panel_4.add(tfEditoraIncluir);
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String titulo, autor, genero, editora;
				Connection conn = null;
				String table = "idLivro";
				ControleID temp = new ControleID();
				try{
					
					ConexaoDb minhaConexao = new ConexaoDb();
					conn = minhaConexao.getConnection();
					conn.setAutoCommit(false);
					
					int temp2 = temp.verificaId(conn, table);
					id = temp2 + 1;
					temp.salvaId(id, conn, table);
					
					titulo = tfTitiloIncluir.getText();
					autor = tfAutorIncluir.getText();
					genero = tfGeneroIncluir.getText();
					editora = tfEditoraIncluir.getText();
					
					Livro livro = new Livro(id,titulo,autor,genero,editora);
					livro.inserirLivro(conn);
					conn.commit();
					
					tfTitiloIncluir.setText("");
					tfAutorIncluir.setText("");
					tfGeneroIncluir.setText("");
					tfEditoraIncluir.setText("");
					
					
				}catch (Exception e){
					e.printStackTrace();
				}
				
			}
		});
		btnAdicionar.setBounds(247, 71, 89, 23);
		panel_4.add(btnAdicionar);
		
		JButton btnVoltarLivro = new JButton("Voltar");
		btnVoltarLivro.setBounds(247, 120, 89, 23);
		panel_4.add(btnVoltarLivro);
		btnVoltarLivro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JPanel Buscar = new JPanel();
		tabbedPane.addTab("Buscar", null, Buscar, null);
		Buscar.setLayout(null);
		
		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBorder(new TitledBorder(null, "Buscar Livro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_5.setBounds(0, 0, 523, 211);
		Buscar.add(panel_5);
		
		JLabel label_5 = new JLabel("T\u00EDtulo");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_5.setBounds(10, 27, 46, 14);
		panel_5.add(label_5);
		
		tfTituloBuscar = new JTextField();
		tfTituloBuscar.setColumns(10);
		tfTituloBuscar.setBounds(54, 25, 86, 20);
		panel_5.add(tfTituloBuscar);
		
		JLabel label_6 = new JLabel("Autor");
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_6.setBounds(10, 66, 46, 14);
		panel_5.add(label_6);
		
		tfAutorBuscar = new JTextField();
		tfAutorBuscar.setColumns(10);
		tfAutorBuscar.setBounds(54, 56, 86, 20);
		panel_5.add(tfAutorBuscar);
		
		JLabel label_7 = new JLabel("G\u00EAnero");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_7.setBounds(10, 104, 46, 14);
		panel_5.add(label_7);
		
		tfGeneroBuscar = new JTextField();
		tfGeneroBuscar.setColumns(10);
		tfGeneroBuscar.setBounds(54, 102, 86, 20);
		panel_5.add(tfGeneroBuscar);
		
		JLabel label_8 = new JLabel("Editora");
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_8.setBounds(10, 146, 46, 14);
		panel_5.add(label_8);
		
		tfEditoraBuscar = new JTextField();
		tfEditoraBuscar.setColumns(10);
		tfEditoraBuscar.setBounds(54, 144, 86, 20);
		panel_5.add(tfEditoraBuscar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnBuscar.setBounds(143, 175, 89, 23);
		panel_5.add(btnBuscar);
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAlterar.setBounds(277, 175, 89, 23);
		panel_5.add(btnAlterar);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.setBounds(411, 175, 89, 23);
		panel_5.add(btnExcluir);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(143, 11, 357, 159);
		panel_5.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id", "T\u00EDtulo", "Autor", "Genero", "Editora"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblSistemaBibliotecrio = new JLabel("Sistema Bibliotec\u00E1rio");
		lblSistemaBibliotecrio.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		lblSistemaBibliotecrio.setBounds(131, 0, 183, 24);
		contentPane.add(lblSistemaBibliotecrio);
	}
}
