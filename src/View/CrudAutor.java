package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CrudAutor extends JFrame {

	private JPanel contentPane;
	private JTextField tfNomeAutor;
	private JTextField tfNAscimentoAutor;
	private JTextField tfCpfAutor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrudAutor frame = new CrudAutor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CrudAutor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 315, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Adicionar Autor", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(0, 0, 318, 261);
		contentPane.add(panel);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNome.setBounds(20, 25, 46, 14);
		panel.add(lblNome);
		
		tfNomeAutor = new JTextField();
		tfNomeAutor.setColumns(10);
		tfNomeAutor.setBounds(112, 23, 86, 20);
		panel.add(tfNomeAutor);
		
		JLabel lblObra = new JLabel("Nascimento");
		lblObra.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblObra.setBounds(20, 74, 69, 14);
		panel.add(lblObra);
		
		tfNAscimentoAutor = new JTextField();
		tfNAscimentoAutor.setColumns(10);
		tfNAscimentoAutor.setBounds(112, 72, 86, 20);
		panel.add(tfNAscimentoAutor);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCpf.setBounds(20, 123, 46, 14);
		panel.add(lblCpf);
		
		tfCpfAutor = new JTextField();
		tfCpfAutor.setColumns(10);
		tfCpfAutor.setBounds(112, 121, 86, 20);
		panel.add(tfCpfAutor);
		
		JButton button = new JButton("Adicionar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setBounds(112, 168, 89, 23);
		panel.add(button);
		
		JButton btnVoltarAutor = new JButton("Voltar");
		btnVoltarAutor.setBounds(112, 227, 89, 23);
		panel.add(btnVoltarAutor);
	}
}
