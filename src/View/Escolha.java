package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class Escolha extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Escolha frame = new Escolha();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Escolha() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLivros = new JButton("Selecionar");
		btnLivros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnLivros.setBounds(166, 73, 104, 23);
		contentPane.add(btnLivros);
		
		JButton btnAutor = new JButton("Selecionar");
		btnAutor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAutor.setBounds(166, 206, 104, 23);
		contentPane.add(btnAutor);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 118);
		contentPane.add(panel);
		
		JLabel lblCliqueParaCadastrar = new JLabel("Clique para cadastrar ou buscar um livro");
		panel.add(lblCliqueParaCadastrar);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 134, 414, 116);
		contentPane.add(panel_1);
		
		JLabel lblCliqueParaCadastrar_1 = new JLabel("Clique para cadastrar um autor");
		panel_1.add(lblCliqueParaCadastrar_1);
	}
}
